﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
//using OpenQA.Selenium.Support.PageObjects;
using SeleniumExtras.PageObjects;
using System.Threading.Tasks;

namespace Lim
{
    class PageObject
    {

        public PageObject()
        {
            PageFactory.InitElements(properties.driver, this);
        }

        //[FindsBy(How = How.XPath, Using = "//a[@class = 'call'] ")]

        //public IWebElement TetxtFrame1 { get; set; }
        // [FindsBy(How = How.XPath, Using = "//input[@id = 'amount'] ")]

        [FindsBy(How = How.XPath, Using = "//a[@class = 'h-c-button h-c-header__cta-li-link h-c-header__cta-li-link--primary'] ")]

        public IWebElement TetxtFrame { get; set; }



        [FindsBy(How = How.XPath, Using = "//input[@id ='generate']")]

        public IWebElement Generatebtn { get; set; }



    }

    class RadioButton
    {
        public RadioButton()
        {
            PageFactory.InitElements(properties.driver, this);
        }

        public void SetValue(string str)
        {
            if (str == "paragraphs")
            {
                Radiobtn.GetAttribute("paragraphs");
                Radiobtn.Click();
            }
            else
                if (str == "words")
            { 
                Radiobtn2.Click(); 
            }
            else
                    if (str == "bytes")
            {
                Radiobtn3.Click();
            }
            else
                if (str == "lists")
            {
                Radiobtn4.Click();
            }
          
                
                
        }
            
        [FindsBy(How= How.XPath,Using = "//input[@type = 'radio']")]
        public IWebElement Radiobtn { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@value = 'paragraphs']")]

        public IWebElement Radiobtn1 { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@value = 'words']")]

        public IWebElement Radiobtn2 { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@value = 'bytes']")]

        public IWebElement Radiobtn3 { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@value = 'lists']")]

        public IWebElement Radiobtn4 { get; set; }

       
    }
    
     

   



}
