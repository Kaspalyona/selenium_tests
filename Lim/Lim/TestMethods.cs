﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lim
{
    class TestMethods
    {
        public void GetUrl (IWebDriver driver, string url)
        {
            driver.Navigate().GoToUrl(url);

        }

        public static void SetWord(IWebDriver driver , string value )
        {
            IWebElement number = driver.FindElement(By.XPath("//input[@id = 'amount'] "));
            number.Clear();
            number.SendKeys(value);
            
            IWebElement word = driver.FindElement(By.XPath("//input[@id ='words']"));
            word.Click();
            IWebElement generate = driver.FindElement(By.XPath("//input[@id ='generate']"));
            generate.Click();
        }

        public static int CalculateWords(IWebDriver driver)
        {
            IWebElement text = driver.FindElement(By.XPath("//div[@id='lipsum']"));         
            return text.Text.Split(' ').Length; 
            
           

        }
    }
}
